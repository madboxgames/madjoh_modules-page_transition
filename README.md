# page transition #

v1.1.0

This module defines primary transition utilities.

## Upgrades since 1.0.0 ##

Since 1.0.0 the only change is the CSS file with the grouping of visible and invisible pages under unique classnames.

## Dependencies ##

For this code to work, you need : 

- to add `<div id="cacheBlock"></<div>` as a parallele to your app pages.
- to import the Styling CSS File (for the cacheBlock).

#### Example ####
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, width=device-width, height=device-height, minimal-ui"/>
        <title>Example</title>
        <link rel="stylesheet" type="text/css" href="../[...]/Styling.css" />
    </head>
    <body>
        <!-- PAGES -->
        <div id="page-container">
            <div id="p_body">
                <!-- GAME PAGE -->
                <div class="Page transitPage activePage">
                    <header class="title-header">
                        <h1>Title</h1>
                    </header>
                    <div class="PageBody">
                 		[...]
                    </div>
                    <footer>
                        <p> Advertising </p>
                    </footer>
                </div>

                <!-- CACHE PAGE -->
                <div id="cacheBlock"></div>
            </div>
        </div>

        <!-- ADVERTISING -->
        <aside id="adSense">[...]</aside>
        
        <!-- SCRIPTS -->
        <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
```

## The Javascript File ##

This file contains 2 methods :

- ** startTransition ** : initializes the transition and blocks the page with the cache block.
- ** endTransition ** : ends the transition and unblocks the page from the cache block.

## The CSS File ##

This file sets to false the visibility of the following class :

- ** class="transitPage" **

It gives back the visibility to pages that are active :

- ** class="activePage" **

It defines the main CSS transitions as follow : ** page[IO][XY] ** where :

IO can be :

- ** InFrom **
- ** OutTo **

XY can be :

- ** Left **
- ** Right **
- ** Top **
- ** Bottom **