define(function(){	
	if(!document.getElementById('cacheBlock')){
		console.log('The cache block page was not found !');
		return;
	}else{
		var PageTransition = {
			activeString : 'activePage',
			activeReg : new RegExp('(\\s|^)'+'activePage'+'(\\s|$)'),

			outTransits : ['pageOutToRight', 'pageOutToBottom', 'pageOutToLeft', 'pageOutToTop'],
			inTransits : ['pageInFromRight', 'pageInFromBottom', 'pageInFromLeft', 'pageInFromTop'],

			init : function(){
				document.addEventListener('deviceready', PageTransition.onDeviceReady, false);
			},
			onDeviceReady : function(){
				document.addEventListener('backbutton', PageTransition.onBackButton, false);
			},
			onBackButton : function(e){
				e.preventDefault();
			},

			startTransition : function(){
				var cacheBlock = document.getElementById('cacheBlock');
				if(cacheBlock) cacheBlock.style.display = 'block';
			},
			endTransition : function(){
				var cacheBlock = document.getElementById('cacheBlock');
				if(cacheBlock) cacheBlock.style.display = 'none';
			}
		};

		return PageTransition
	}
});